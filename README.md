Afiliación de hospitales

Todo el mundo tiene una idea bastante clara de lo que es una fusión o una adquisición, pero el uso de la palabra "afiliación" siempre me hizo sentir que no había hecho del todo mi trabajo como periodista.

¿Han inventado los directivos de los hospitales un nuevo tipo de asociación? ¿O se trata de semántica para evitar el pánico que sienten los empleados y las comunidades cuando se pronuncian las palabras "fusión" o "adquisición"?

Llevo dos años buscando la respuesta a esta pregunta y he llegado a una conclusión poco convincente: Sí, a veces es sólo semántica, pero las afiliaciones también pueden ser una estructura de asociación independiente con diversos grados de mezcla.

https://descargarcertificado.net/afiliacion-capital-salud/